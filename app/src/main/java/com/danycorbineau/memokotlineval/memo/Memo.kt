package com.danycorbineau.memokotlineval.memo

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Memo(memoText: String) {

    @JvmField
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    @ColumnInfo(name = "text")
    var text: String? = memoText

    constructor() : this("")
}