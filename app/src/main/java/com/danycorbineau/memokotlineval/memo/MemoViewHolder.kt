package com.danycorbineau.memokotlineval.memo

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class MemoViewHolder(adapter: MemoAdapter, memoV: View) : RecyclerView.ViewHolder(memoV) {
    lateinit var memo: Memo
    var memoView: View = memoV

    init {
        memoView.setOnClickListener {
            adapter.savePosition(adapterPosition)
            adapter.showDetail(memo.text)
        }
    }
}