package com.danycorbineau.memokotlineval.memo

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.danycorbineau.memokotlineval.di.DIApplication
import javax.inject.Inject

class MemoListViewModel() : ViewModel() {

    @Inject
    lateinit var memoDAO: MemoDAO

    private var liveDataList: MutableLiveData<MutableList<Memo>> = MutableLiveData()
    private var activeList: ArrayList<Memo> = ArrayList()

    init {
        DIApplication.instance.getAppComponent().inject(this)
    }

    fun initialisation() {
        activeList.clear()
        memoDAO.getAll().forEach { activeList.add(it) }
    }

    fun getLiveDataList(): MutableLiveData<MutableList<Memo>> {
        return liveDataList
    }

    fun getList(): MutableList<Memo> {
        return activeList
    }

    fun buttonAddMemo(memoText: String) {
        val memo = Memo(memoText)
        memoDAO.insertAll(memo)
        activeList.add(memo)
        liveDataList.value = activeList
    }

    fun removeMemo(memo: Memo) {
        activeList.remove(memo)
        memoDAO.delete(memo)
        liveDataList.value = activeList
    }

}