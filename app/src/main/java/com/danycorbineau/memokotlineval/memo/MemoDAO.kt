package com.danycorbineau.memokotlineval.memo

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface MemoDAO {
    @Query("SELECT * FROM memo")
    fun getAll(): MutableList<Memo>

    @Insert
    fun insertAll(vararg memos: Memo)

    @Delete
    fun delete(memo: Memo)

}