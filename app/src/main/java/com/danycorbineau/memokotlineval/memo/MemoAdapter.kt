package com.danycorbineau.memokotlineval.memo

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import com.danycorbineau.memokotlineval.detail.DetailActivity
import com.danycorbineau.memokotlineval.detail.DetailFragment
import com.danycorbineau.memokotlineval.MainActivity
import com.danycorbineau.memokotlineval.R
import kotlinx.android.synthetic.main.memo_item_list.view.*

class MemoAdapter(
    private val memoListViewModel: MemoListViewModel,
    private val mainActivity: MainActivity
) : RecyclerView.Adapter<MemoViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MemoViewHolder {
        val memo: View =
            LayoutInflater.from(parent.context).inflate(R.layout.memo_item_list, parent, false)
        return MemoViewHolder(this, memo)
    }

    override fun getItemCount(): Int {
        return memoListViewModel.getList().size
    }

    override fun onBindViewHolder(holder: MemoViewHolder, position: Int) {
        val memo: Memo = memoListViewModel.getList()[position]
        holder.memoView.memo_text.text =
            if (memo.text?.length!! > 20) memo.text?.substring(0, 20) else memo.text
        holder.memo = memoListViewModel.getList()[position]
    }

    fun showDetail(detail: String?) {
        if (
            getScreenWidthDB() > 600
            && mainActivity.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE
        ) {
            val detailFragment = DetailFragment()
            val i = Intent()
            i.putExtra(DetailFragment.TEXT_FIELD_KEY, detail)
            detailFragment.arguments = i.extras
            mainActivity.supportFragmentManager.beginTransaction()
                .add(R.id.detail_frame_layout, detailFragment).commit()
        } else {
            val i = Intent(mainActivity, DetailActivity::class.java)
            i.putExtra(DetailFragment.TEXT_FIELD_KEY, detail)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            mainActivity.startActivity(i)
        }
    }

    fun savePosition(position: Int) {
        val sp = PreferenceManager.getDefaultSharedPreferences(mainActivity)
        val e = sp.edit()
        e.putInt(MainActivity.LAST_POSITION_KEY, position)
        e.apply()
    }

    private fun getScreenWidthDB(): Int {
        val wm = mainActivity.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val metrics = DisplayMetrics()
        wm.defaultDisplay.getMetrics(metrics)
        return (metrics.widthPixels / (mainActivity.resources.displayMetrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT))
    }
}