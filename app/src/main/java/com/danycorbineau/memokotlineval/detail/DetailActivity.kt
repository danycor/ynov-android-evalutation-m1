package com.danycorbineau.memokotlineval.detail

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.danycorbineau.memokotlineval.R

class DetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_layout)
        if (findViewById<View?>(R.id.detail_frame_layout) != null) {
            if (savedInstanceState != null) {
                return
            }
            val detailFragment = DetailFragment()
            detailFragment.arguments = intent.extras
            supportFragmentManager.beginTransaction()
                .add(R.id.detail_frame_layout, detailFragment).commit()
        }
    }
}