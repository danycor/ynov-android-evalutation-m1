package com.danycorbineau.memokotlineval.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.danycorbineau.memokotlineval.R
import kotlinx.android.synthetic.main.detail_fragment.view.*

class DetailFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.detail_fragment, container, false)
        if (arguments != null) {
            val detail = requireArguments().getString(TEXT_FIELD_KEY)
            v.detail_memo.text = detail
        }
        return v
    }

    companion object {
        const val TEXT_FIELD_KEY = "TEXT_FIELD_KEY"
    }
}