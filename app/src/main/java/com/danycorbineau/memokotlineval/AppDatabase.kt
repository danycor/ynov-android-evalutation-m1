package com.danycorbineau.memokotlineval

import androidx.room.Database
import androidx.room.RoomDatabase
import com.danycorbineau.memokotlineval.memo.Memo
import com.danycorbineau.memokotlineval.memo.MemoDAO

@Database(entities = [Memo::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun memoDao(): MemoDAO
}