package com.danycorbineau.memokotlineval.di

import android.content.Context
import com.danycorbineau.memokotlineval.DAO
import com.danycorbineau.memokotlineval.memo.MemoDAO
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @Provides
    fun getDao(context: Context): MemoDAO {
        return DAO.getDatabase(context).memoDao()
    }

    @Provides
    fun getContext(): Context {
        return DIApplication.instance;
    }

}