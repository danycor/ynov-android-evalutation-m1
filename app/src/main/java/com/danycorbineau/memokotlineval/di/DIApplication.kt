package com.danycorbineau.memokotlineval.di

import android.app.Application
import com.danycorbineau.memokotlineval.MainActivity
import com.danycorbineau.memokotlineval.memo.MemoListViewModel
import dagger.Component

@Component(modules = [AppModule::class])
interface ApplicationComponent {
    fun inject(memoModelViewModel: MemoListViewModel)
    fun inject(mainActivity: MainActivity)
}

class DIApplication : Application() {
    private object HOLDER {
        val INSTANCE = DIApplication()
    }

    private var appComponent: ApplicationComponent? = null

    fun getAppComponent(): ApplicationComponent {
        if (appComponent == null) {
            appComponent = DaggerApplicationComponent.create()
        }
        return this.appComponent!!
    }

    companion object {
        val instance: DIApplication by lazy { HOLDER.INSTANCE }
    }
}
