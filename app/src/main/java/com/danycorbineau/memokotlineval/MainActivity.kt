package com.danycorbineau.memokotlineval

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.danycorbineau.memokotlineval.memo.Memo
import com.danycorbineau.memokotlineval.memo.MemoAdapter
import com.danycorbineau.memokotlineval.memo.MemoItemTouchHelperCallback
import com.danycorbineau.memokotlineval.memo.MemoListViewModel
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception

class MainActivity : AppCompatActivity() {
    private lateinit var memoListViewModel: MemoListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        DAO.getDatabase(this)

        setUpMemoListViewModel()
        setUpListeners()
        setUpRecyclerView()
        toastLastPosition()
    }

    fun setUpMemoListViewModel() {
        memoListViewModel = ViewModelProvider(this).get(MemoListViewModel::class.java)
        memoListViewModel.initialisation()
    }

    private fun setUpListeners() {
        memoListViewModel.getLiveDataList().observe(this,
            Observer<List<Memo>> {
                Log.i("DCLOG", "Notify list change")
                memo_recycler_view.adapter?.notifyDataSetChanged()
            })

        memo_add_button.setOnClickListener {
            try {
                memoListViewModel.buttonAddMemo(memo_add_field.text.toString())
                Toast.makeText(
                    this,
                    resources.getString(R.string.memo_add_text) + memo_add_field.text.toString(),
                    Toast.LENGTH_SHORT
                ).show()
                memo_add_field.setText("")
            } catch (e: Exception) {
                Toast.makeText(
                    this,
                    resources.getString(R.string.memo_add_error_text) + e.message,
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun toastLastPosition() {
        val sp = PreferenceManager.getDefaultSharedPreferences(this)
        val value = sp.getInt(LAST_POSITION_KEY, -1)
        if (value >= 0) {
            val toast = Toast.makeText(
                this,
                resources.getString(R.string.memo_last_position_clic_text) + value,
                Toast.LENGTH_SHORT
            )
            toast.show()
        }
    }

    private fun setUpRecyclerView() {
        memo_recycler_view.setHasFixedSize(true)
        memo_recycler_view.layoutManager = LinearLayoutManager(this)
        memo_recycler_view.adapter = MemoAdapter(memoListViewModel, this)

        ItemTouchHelper(MemoItemTouchHelperCallback(memoListViewModel)).attachToRecyclerView(
            memo_recycler_view
        )
    }

    companion object {
        const val LAST_POSITION_KEY = "last_position_key"
    }
}
