package com.danycorbineau.memokotlineval

import android.content.Context
import androidx.room.Room

class DAO(context: Context) {
    private object HOLDER {
        var INSTANCE: DAO? = null
    }

    private var appDatabase: AppDatabase

    init {
        appDatabase = Room.databaseBuilder(context, AppDatabase::class.java, "memo.db")
            .allowMainThreadQueries().build()
    }


    companion object {
        fun getDatabase(context: Context): AppDatabase {
            if (HOLDER.INSTANCE === null)
                HOLDER.INSTANCE = DAO(context.applicationContext)
            return HOLDER.INSTANCE!!.appDatabase
        }
    }
}