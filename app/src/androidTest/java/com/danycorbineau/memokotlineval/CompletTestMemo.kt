package com.danycorbineau.memokotlineval

import androidx.test.espresso.Espresso
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.*
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class CompletTestMemo {
    @get:Rule
    var activityRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)

    @Test
    fun memoTest() {
        val memoText = "Memo test" + (Math.random() * 1000.0f).toInt()
        Espresso.onView(ViewMatchers.withId(R.id.memo_add_field))
            .perform(
                ViewActions.typeText(memoText), ViewActions.closeSoftKeyboard()
            )
        Espresso.onView(ViewMatchers.withId(R.id.memo_add_button))
            .perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withText(memoText)).check(
            ViewAssertions.matches(ViewMatchers.withText(memoText))
        )
        Espresso.onView(ViewMatchers.withText(memoText))
            .perform(ViewActions.scrollTo(), ViewActions.swipeRight())
        Thread.sleep(200)
    }

    fun getSlowSwipeAction(): ViewAction? {
        return GeneralSwipeAction(
            Swipe.FAST,
            GeneralLocation.CENTER_LEFT,
            GeneralLocation.CENTER_RIGHT,
            Press.FINGER
        )
    }

}